#![feature(specialization)]

#[macro_use]
extern crate pyo3;
// Imports
use pyo3::exceptions;
use pyo3::prelude::*;
use std::net::IpAddr::{V4, V6};
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};

/// Main IP checking struct. Stores the prefixes for later use. Only really meant to be used as a CPython class.
#[pyclass]
struct FastIPChecker {
    prefixes_v4: Vec<(u32, usize)>,
    prefixes_v6: Vec<(u128, usize)>,
}

/// Python implementations of checker methods
#[pymethods]
impl FastIPChecker {
    /// Python init method. End goal is to parse all prefixes into a tuple of (start ip, prefix length)
    #[new]
    fn __new__(obj: &PyRawObject, prefixes: Vec<String>) -> PyResult<()> {
        /// Variable declaration
        let mut p4 = Vec::<(u32, usize)>::new();
        let mut p6 = Vec::<(u128, usize)>::new();
        for p in &(prefixes) {
            match (*p).find('/') { // Have to deref since this is borrowed from the Vec<String>
                None => { // No prefix length specified.
                    return Err(exceptions::ValueError::py_err(format!(
                        "Invalid prefix: {}",
                        p
                    )))
                }
                Some(idx) => match p[idx+1..].parse::<usize>() { // We have the index, now to get the prefix length using said index.
                    Err(_) => { // That's not an integer.
                        return Err(exceptions::ValueError::py_err(format!(
                            "Invalid prefix: {}",
                            p
                        )))
                    }
                    Ok(s) => match (p[..idx]).parse::<IpAddr>() { // That is an integer. Time to parse everything else as an IP addr.
                        Err(_) => { // That's not an IP.
                            return Err(exceptions::ValueError::py_err(format!(
                                "Invalid prefix: {}",
                                p
                            )))
                        }
                        Ok(p) => match p { // That is an IP. Match on v4/v6.
                            V4(p) => { // It's v4.
                                if s <= 32 {
                                    p4.push((u32::from(p), 32 - s)) // Add this prefix to the vector.
                                } else { // Prefix length longer than 32. Something is very wrong here.
                                    return Err(exceptions::ValueError::py_err(format!(
                                        "Invalid prefix length {} for prefix {}",
                                        s, p
                                    )));
                                }
                            }
                            V6(p) => {
                                if s <= 128 { // Add this prefix to the vector.
                                    p6.push((u128::from(p), 128 - s))
                                } else { // Prefix length longer than 128. Something is very wrong here.
                                    return Err(exceptions::ValueError::py_err(format!(
                                        "Invalid prefix length {} for prefix {}",
                                        s, p
                                    )));
                                }
                            }
                        },
                    },
                },
            }
        }
        obj.init(|_| FastIPChecker { // Init a struct with the prefixes and give it to Python. Final return value.
            prefixes_v4: p4,
            prefixes_v6: p6,
        })
    }

    pub fn check(&self, ip: String) -> PyResult<bool> {
        match ip.parse::<IpAddr>() { // Is this an IP?
            Ok(ip) => match ip { // Yes, it's an IP. Sending it to the appropriate method.
                IpAddr::V4(ip) => Ok(self.check_v4(ip)),
                IpAddr::V6(ip) => Ok(self.check_v6(ip)),
            },
            Err(_) => Err(exceptions::ValueError::py_err("Invalid IP address!")), // Nope. Go away.
        }
    }
}

impl FastIPChecker {
    /// Checker for v6 addrs
    fn check_v6(&self, ip: Ipv6Addr) -> bool { 
        let ip_bits: u128 = u128::from(ip); // Get IP bits
        for prefix in &(self.prefixes_v6) { // O(n)
            let (prefix, shiftlen) = *prefix; // Deref, now we have prefix and its length
            if (prefix >> shiftlen) == (ip_bits >> shiftlen) { // Shift out the unfixed bits and compare them.
                return true;
            }
        }
        false // Didn't match any of the prefixes.
    }
    /// Checker for v4 addrs
    fn check_v4(&self, ip: Ipv4Addr) -> bool {
        let ip_bits: u32 = u32::from(ip); // Get IP bits
        for prefix in &(self.prefixes_v4) { // O(n)
            let (prefix, shiftlen) = *prefix; // Deref, now we have prefix and its length
            if (prefix >> shiftlen) == (ip_bits >> shiftlen) { // Shift out the unfixed bits and compare them.
                return true;
            }
        }
        false // Didn't match any of the prefixes.
    }
}

/// PyO3 stuff.
#[pymodinit]
fn fast_net_checker(_py: Python, m: &PyModule) -> PyResult<()> {
    let _ = m.add_class::<FastIPChecker>();
    Ok(())
}
